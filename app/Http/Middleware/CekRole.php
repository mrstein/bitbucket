<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$roleName)
    {
        $data=Auth::user()->hasRole($roleName);
        if($data)
        {
            return $next($request);
        }
        return abort(503, "Anda tidak mempunyai akses ke sini");
      
    }
}
