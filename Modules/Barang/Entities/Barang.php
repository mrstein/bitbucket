<?php

namespace Modules\Barang\Entities;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public $table="barangs";
    protected $fillable=["kode_barang","nama_barang","harga_jual","harga_beli","stok"];

    
}
