@extends('layouts.app')

@section('datatable_css')
<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection


@section('content')
<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Daftar Barang
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                   	
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="tab-content">
                       @if (session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('success') }}
                                </div>
                        @endif
                    <a href="{{route('barang.form_add')}}"><button class="btn btn-primary">Tambah</button></a>
                    <br>
                    <br>
                    <div class="tab-pane active" style="overflow-x:auto;" id="kt_widget11_tab1_content">
                    <table id="tabel_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Kode Barang</th>
                          <th>Nama Barang</th>
                          <th>Harga Beli</th>
                          <th>Harga Jual</th>
                          <th>Jumlah</th>
                        <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                                                      
                      </tbody>
                    </table>
                       <br>
                       <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End::Section-->



<!--End::Dashboard 4-->
@endsection
@section("datatable")
<script src="{{ asset('js/sweetalert.min.js')}}"></script>
<script src="{{ asset('js/axios.js')}}"></script>
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/custom/general/crud/datatables/basic/scrollable.js') }}" type="text/javascript"></script>
<script>
  var token = document.head.querySelector('meta[name="csrf-token"]');
     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

     function getData(){
       $('#tabel_barang').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/barang/datatable",
        columns: [
            {data: 'kode_barang', name: 'kode_barang'},
            {data: 'nama_barang', name: 'nama_barang'},
            {data: 'harga_beli', name: 'harga_beli'},
            {data: 'harga_jual', name: 'harga_jual'},
            {data: 'stok', name: 'stok'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
  }

  function showAlert(id){
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          axios.delete("/barang/"+id+"/delete").then((response)=>{
             swal("Data Barang sudah dihapus!", {
             icon: "success",
            });
            //getData();
            location.reload();
          }).catch((err)=>{
            console.log("Gagal : "+err);
            swal("Data Barang gagal dihapus!", {
             icon: "warning",
            });
          })
        }
      });
    }
    getData();

</script>
@endsection