<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('barang')->group(function() {
    Route::get('/', 'BarangController@index');
    Route::get('/datatable', 'BarangController@Datatable')->middleware(['auth','roles:Admin,Apoteker']);;
    Route::get('/list', 'BarangController@listbarang')->name("barang.view")->middleware(['auth','roles:Admin,Apoteker']);;
    Route::get('/add', 'BarangController@FormAdd')->name("barang.form_add")->middleware(['auth','roles:Admin,Apoteker']);;
    Route::post('/add', 'BarangController@store')->name("barang.store")->middleware(['auth','roles:Admin,Apoteker']);;
    Route::get('/{id}/edit', 'BarangController@FormEdit')->name("barang.form_edit")->middleware(['auth','roles:Admin,Apoteker']);;
    Route::post('/{id}/edit', 'BarangController@update')->name("barang.update")->middleware(['auth','roles:Admin,Apoteker']);;
    Route::delete('/{id}/delete', 'BarangController@destroy')->name("barang.destroy")->middleware(['auth','roles:Admin,Apoteker']);
});
