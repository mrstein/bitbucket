<?php

namespace Modules\Barang\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Modules\Barang\Entities\Barang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BarangController extends Controller
{
    use ValidatesRequests; 

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('barang::index');
    }

    public function listbarang()
    {
        return view('barang::show');
    }

    public function Datatable(){
        $barang = DB::table("barangs")->select(['id', 'kode_barang', 'nama_barang', DB::raw('FORMAT(harga_beli,0) as harga_beli'), DB::raw('FORMAT(harga_jual,0) as harga_jual'), DB::Raw('FORMAT(stok,0) as stok')]);
        return Datatables::of($barang)
        ->addColumn('action', function ($user) {
            return '<a href="'.route("barang.form_edit",$user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp&nbsp'.
                     '<button onclick="showAlert(this.id)" id="'.$user->id.'"  class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-remove"></i> Hapus</a>';
        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('barang::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_barang' => 'required|unique:barangs',
            'nama_barang' => 'required',
            'harga_jual' => 'required|numeric',
            'harga_beli' => 'required|numeric',
            'stok' => 'required|numeric'
         ]);

         Barang::create([
             "kode_barang"=>$request->kode_barang,
             "nama_barang"=>$request->nama_barang,
             "harga_jual"=>$request->harga_jual,
             "harga_beli"=>$request->harga_beli,
             "stok"      =>$request->stok,
         ]);
         return redirect()->route("barang.view")->with("success","Berhasil input data barang !");
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('barang::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('barang::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_barang' => 'required',
            'harga_jual' => 'required|numeric',
            'harga_beli' => 'required|numeric',
            'stok' => 'required|numeric'
         ]);
         $data=Barang::find($id);
         $data->kode_barang=$request->kode_barang;
         $data->nama_barang=$request->nama_barang;
         $data->harga_beli=$request->harga_beli;
         $data->harga_jual=$request->harga_jual;
         $data->stok=$request->stok;
         $data->save();
         return redirect()->route("barang.view")->with("success","Berhasil edit data barang !");
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Barang::find($id)->delete();
        return response()->json(["success"=>0,"message"=>"Berhasil hapus data !"]);
        //
    }
    
    public function FormAdd(){
        return view("barang::add");
    }
    public function FormEdit($id)
    {
        $data=Barang::find($id);
        return view('barang::edit',compact("data"));
    }
}
