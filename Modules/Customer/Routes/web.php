<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('customer')->group(function() {
    Route::get('/', 'CustomerController@index');
    Route::get('/list', 'CustomerController@list')->name("customer.view")->middleware(['auth','roles:Admin,Kasir']);
    Route::get('/datatable', 'CustomerController@datatable')->middleware(['auth','roles:Admin,Kasir']);;
    Route::get('/add', 'CustomerController@FormAdd')->name("customer.form_add")->middleware(['auth','roles:Admin,Kasir']);;
    Route::post('/add', 'CustomerController@store')->name("customer.store")->middleware(['auth','roles:Admin,Kasir']);;
    Route::get('/{id}/edit', 'CustomerController@FormEdit')->name("customer.form_edit")->middleware(['auth','roles:Admin,Kasir']);;
    Route::post('/{id}/edit', 'CustomerController@update')->name("customer.update")->middleware(['auth','roles:Admin,Kasir']);;
    Route::delete('/{id}/delete', 'CustomerController@destroy')->name("customer.destroy")->middleware(['auth','roles:Admin,Kasir']);;

});
