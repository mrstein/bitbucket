@extends('layouts.app')


@section('content')
<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Customer
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                   	
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="tab-content">
                        @if (session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('success') }}
                                </div>
                        @endif

                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
						<div class="tab-pane active" id="kt_widget11_tab1_content">
                          <form class="kt-form" method="POST" action="{{ route('customer.update',$data->id)}}">
                            {{ csrf_field() }}
							<div class="kt-portlet__body">
                              <div class="kt-section kt-section--first">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nama</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="name" value="{{ $data->name }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Telepon</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" value="{{ $data->phone }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Alamat</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="address" value="{{ $data->address }}" class="form-control">
                                        </div>
                                    </div>
                               
                               </div>     
                            </div>
                            <div class="kt-portlet__foot">
							    <div class="kt-form__actions">
									<div class="row">
									  <div class="col-lg-3"></div>
									  <div class="col-lg-6">
											<button  type="submit" class="btn btn-success">Submit</button>
									  </div>
									</div>
							    </div>
							</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End::Section-->



<!--End::Dashboard 4-->
@endsection
