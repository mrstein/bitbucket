<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;




class CustomerController extends Controller
{
    use ValidatesRequests; 

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('customer::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('customer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required'
         ]);

         Customer::create([
             "name"=>$request->name,
             "phone"=>$request->phone,
             "address"=>$request->address,
         ]);
         return redirect()->route("customer.view")->with("success","Berhasil input data customer !");
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('customer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('customer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required'
     ]);
     $data=Customer::find($id);
     $data->name=$request->name;
     $data->phone=$request->phone;
     $data->address=$request->address;
     $data->save();
     return redirect()->route("customer.view")->with("success","Berhasil edit data customer !");
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Customer::find($id)->delete();
        return response()->json(["success"=>0,"message"=>"Berhasil hapus data !"]);
        //
    }

    public function datatable(){
        $data=DB::table("customer")->select(["id","name","phone","address"]);
        return Datatables::of($data)
        ->addColumn('action', function ($user) {
            return '<a href="'.route("customer.form_edit",$user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp&nbsp'.
                     '<button onclick="showAlert(this.id)" id="'.$user->id.'"  class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-remove"></i> Hapus</a>';
        })->make(true);
    }

    public function list(){
        return view('customer::show');

    }
    public function FormAdd(){
        return view("customer::add");
    }

    public function FormEdit($id)
    {
        $data=Customer::find($id);
        return view('customer::edit',compact('data'));
    }


}
