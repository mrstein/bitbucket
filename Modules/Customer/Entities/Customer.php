<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $table="customer";
    protected $fillable = ["id","name","phone","address"];
    public $timestamps=false;
}
